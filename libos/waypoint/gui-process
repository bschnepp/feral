[[work in progress, this is a long ways away, considering building this to work on Linux and then port (back) later.]]

Applications need a few things from the OS:
	System State (WpSystemState*)
	Global Application State (WpGlobalApplicationState*)


System state contains a few important things:
	- Mouse, keyboards, joysticks, controllers, etc. are all queried from there. (important for avoiding keyloggers and whatnot. Your app takes a handle to a *very specific peripheral*.)
	- Monitors can be queried from it.
	- Resolution (actual) and resolution (virtual) can be queried from it. Actual is the _real_ resolution of the monitor, whereas virtual is how much it thinks it is. (ie, 4K on a 1080p monitor)
		- (this is important as it allows an application to scale better in situations where one forces a higher resolution than they actually have.)

	Importantly, handles to mice/keyboard/etc are done by a unique identifier connected to the OS' subsystem for peripheral management. It doesn't rely upon the device _remaining_ there, only being
	used while it is there. The handle should remain valid if the device is removed and then inserted back in, but only while it is still there.
		(Need some other behavior too to avoid having the "render cursor on screen of game after you used a USB switcher" bug that lots of games have: we're the OS, we can do whatever we want to
		 fix broken applications.)

Global Application State contains some other info:
	- How do we draw buttons? Frames? Dark theme or bright theme? 
	- What colors do we use to draw widgets?
	- What application handles audio? (analogue to PulseAudio? Should we just use the kernel for audio? etc. etc.)
	- Do we have a CPU emulator available, if our "game" is really a wrapper to emulate a game console (useful for PPC32, MIPS, etc. based consoles which need an emulator brought over.)
	- What is the class structure of widgets? (HAPPLICATIONFRAME, HFRAME, HPANEL, HPANELLAYOUT, etc. Some mix of Win32, GTK, and Swing "style", while trying to maintain that "superset of Vulkan feel").

From here, we create an application frame (and can have sub-frames ala the GNU GIMP, but all GUI apps *must* have an ApplicationFrame), From there, the hierarchy for class structure can be found.
(These are null-terminated strings, and stuff like HBUTTON, HMENU, HSUBMENU, etc. can be found at each step.)

New widgets can be created, with something along the lines of sub-classing this:

typedef enum
{
	WIDGET_TYPE_FRAME,
	WIDGET_TYPE_PANEL,
	WIDGET_TYPE_BUTTON,
	WIDGET_TYPE_MENU,
	WIDGET_TYPE_SCROLLPANE,
}WidgetType;

typedef struct
{
	WidgetType type;

	BOOL widgetEnabled;
	BOOl widgetVisible;

	UINT64 PreferredHeight;
	UINT64 PreferredWidth;
	
	UINT64 MinHeight;
	UINT64 MinWidth;

	UINT64 MaxHeight;
	UINT64 MaxWidth;
	
	VOID (*repaint)(HGRAPHICS g);
	// TODO...
}HWIDGET;


Waypoint does not create an arbitrary widget limitation, the system will allow up to 2^64 different widgets per application.
This should be more than enough for a whole lifetime, and you should never come close to filling up all of those. (if you do, you're not freeing your widgets, which is REALLY BAD.)


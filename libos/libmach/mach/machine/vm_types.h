/*
Copyright (c) 2018, Brian Schnepp

Permission is hereby granted, free of charge, to any person or organization 
obtaining  a copy of the software and accompanying documentation covered by 
this license (the "Software") to use, reproduce, display, distribute, execute, 
and transmit the Software, and to prepare derivative works of the Software, 
and to permit third-parties to whom the Software is furnished to do so, all 
subject to the following:

The copyright notices in the Software and this entire statement, including 
the above license grant, this restriction and the following disclaimer, must 
be included in all copies of the Software, in whole or in part, and all 
derivative works of the Software, unless such copies or derivative works are 
solely in the form of machine-executable object code generated by a source 
language processor.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE FOR ANY 
DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
 */

#ifndef _FERAL_MACH_VM_TYPES_EMU_H_
#define _FERAL_MACH_VM_TYPES_EMU_H_

#include <feral/stdtypes.h>
#include <stdint.h>

typedef UINTN natural_t;
typedef INTN integer_t;

// A certain Mach derivative uses uintptr_t over natural_t (for a lot of reasons).
// Someone may graft a emulation layer for this successful commerical desktop OS on top of our libmach, and thus we need to support that too.
// (We need to be able to support _everything_, provided someone else feels like writing a process handler and a corresponding driver.)
#if defined(__LP64__)
typedef uintptr_t	vm_offset_t;
typedef uintptr_t	vm_size_t;
#else
typedef	natural_t	vm_offset_t;
typedef natural_t	vm_size_t;
#endif

typedef INT32 int32;
typedef UINT32 uint32;

typedef INT8 signed8_t;
typedef INT16 signed16_t;
typedef INT32 signed32_t;
typedef INT64 signed64_t;

typedef UINT8  unsigned8_t;
typedef UINT16 unsigned16_t;
typedef UINT32 unsigned32_t;
typedef UINT64 unsigned64_t;

typedef FLOAT float32_t;
typedef DOUBLE float64_t;
 

#endif


